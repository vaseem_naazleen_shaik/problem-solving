/* Problem Statement :
Find the missing element in a array */

import java.util.*;

public class MissingNum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array: ");
        int n = sc.nextInt();
        // reading size of the array
        int[] arr = new int[n];
        System.out.println("Enter the elements of the array: ");
        // reading n elemnts of array
        for (int i = 0; i < n; i ++) {
            arr[i] = sc.nextInt();
        }

        Arrays.sort(arr);
        for (int i = 0; i < n; i ++) {
            if (arr[i] >= 0) {
               if (i == n - 1) {
                   System.out.println(arr[i] + 1);  
                   break;                 
               }
               if (arr[i] + 1 != arr[i + 1]){
                   System.out.println(arr[i] + 1);
                   break;
               }
            }
        }

     
    }
}
