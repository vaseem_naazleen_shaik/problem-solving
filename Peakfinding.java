// 1 - D peak finding

/*
Problem Statement :
find a peak of the one dimensional array

peak -> it should have its neighbours/neighbour less than it 
    
Time complexity : O(log n)

*/


import java.util.*;

public class Peakfinder {
    static int peakFinder (int[] arr, int start, int stop, int n) {
        int mid = (start + stop) / 2;
        if ((mid == 0 || arr[mid] >= arr[mid - 1]) && (mid == n - 1 ||arr[mid] >= arr[mid + 1])) {
            return arr[mid];
        }
        else if (mid > 0 && arr[mid] < arr[mid + 1]) {
            return Peakfinder.peakFinder(arr, mid + 1, stop, n);
        }
        else {
            return Peakfinder.peakFinder(arr, start, mid - 1, n);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the length of array:");
        int n = sc.nextInt();
        System.out.println("Enter the elements of array:");
        int[] arr = new int[n];
        for (int i = 0; i < n; i ++) {
            arr[i] = sc.nextInt();
        }
        System.out.println(Peakfinder.peakFinder(arr, 0, n - 1, n));
    }
}
