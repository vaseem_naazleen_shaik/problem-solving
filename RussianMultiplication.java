
//Russian multiplication

import java.util.*;
public class RussianMultiplication {

    public static int russe(int a, int b) {

    /*  ArrayList<Integer> x = new ArrayList<Integer>();
      ArrayList<Integer> y = new ArrayList<Integer>();

      x.add(a);
      y.add(b);

      int i = 0;
      while(x.get(i)> 1) {          //until multiplier becomes 1
        x.add(x.get(i) / 2);        //divide multiplier by 2
        y.add(y.get(i) * 2);     // multiply multiplicand by 2
        i += 1;
      }

      int prod = 0;
      while(i >= 0) {
        if(x.get(i) % 2 != 0) {
          prod += y.get(i);             //sum up all multiplicand if the multiplier is odd
        }
        i -= 1;
      }
      return prod; */

      //----------------------------------------------------
      int prod = 0;
      while(a >= 1) {
        System.out.print(a + "  " + b);
        if(a % 2 != 0) {
          System.out.println("  +  ");
          prod += b;
        }
        else{
          System.out.println();
        }
        a = a / 2;
        b = b * 2;
      }

      return prod;
    }

    public static void main(String[] args) {
       Scanner scan = new Scanner(System.in);
       System.out.println("Enter the value of multiplier: ");
       int a = scan.nextInt();
       System.out.println("Enter the value of multiplicand: ");
       int b = scan.nextInt();

       System.out.println("The product of " + a + " and " + b + " = " + russe(a, b));

    }

}

