/* Problem Statement : Given a number n return Fib(n) */

//recursive Algorithm --> To slow

/*if n = 0: return 0
if n = 1: return 1
return fib1(n − 1) + fib1(n − 2)*/


//efficent algorithm.. (dynamic programming)

/*if n = 0 return 0
create an array f[0 . . . n]
f[0] = 0, f[1] = 1
for i = 2 . . . n:
f[i] = f[i − 1] + f[i − 2]
return f[n]*/

public class Fibonacci {
    public static long fib (int n) {
        long [] f = new long[n + 1];
        f[0] = 0L;
        f[1] = 1L;
        for (int i = 2; i <= n ; i ++) {
            f[i] = f[i - 1] + f[i - 2];
        }
        return f[n];
    }
    
    public static void main(String[] args) {
      int n = Integer.parseInt(args[0]); // takes a command line argument 
      System.out.print(Fibonacci.fib(n));
    }
}
