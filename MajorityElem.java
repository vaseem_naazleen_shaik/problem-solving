/* Problem Statement : Find the majority Element in the given array
Majority element -> which repeats more than n / 2 times in array where n is the length of the array */


import java.util.*;
public class MajorityElem {
    public static int  majorityElem(int[] arr, int n) {
        int maj_elem = -1;
        Arrays.sort(arr);
        int i = 0;
        while (i < n - 1) {
           int count = 1;
           while(arr[i] == arr[i + 1]) {
               i += 1;
               count += 1;
               if (i == n - 1) {
                   if (arr[n - 1] == arr[i - 1]) {
                       count += 1;
                       break;
                   }
               }
           }
           if (count > n / 2) {
               return arr[i];
           }
           i += 1;
        }
        return -1;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of array: ");
        int n = sc.nextInt();
        System.out.println("Enter the array elements: ");
        int[] arr = new int[n];
        for (int i = 0; i < n; i ++) {
            arr[i] = sc.nextInt();
        }
        int maj_elem = majorityElem(arr, n);
             
        if (maj_elem == -1) {
          System.out.println("No majority elemet");
        }
        else {
            System.out.print("Majority element: ");
            System.out.println(maj_elem);
        }
    }
}
