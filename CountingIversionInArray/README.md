# Program for Counting Inversion in an Array

### Input :

 An array A of length **n** which has 1, 2, 3 ... n numbers in some arbitrary order

### Output :

No of inversions = No of pairs (i, j) of array indicies with i < j  &  A[ i ] > A[ j ]

##### Example :
 Input : A = [1, 3, 5, 2, 4, 6]

 Output : 3  --> (3, 2), (5, 2), (5, 4)

 ## Approch - 1 :

 The naive algorithm for this problem is to use two loops and count all the inversion. The complexity of this approach would be O(n^2). Though for small number of input, it runs quickly but for large value of n, it takes much time.

  #### Time complexity : O(n^2)

 ## Approch - 2 :

 Using Divide & Conquer Paradigm. (**Merge sort**)

 * **Divide**      :  Divide the array in two parts a[0] to a[n/2] and a[n/2+1] to a[n].
                 
 * **Conquer**  :  Conquer the sub-problem by solving them recursively.
 
 * **Combine**  :  Combine the sub-problem to get the final result.

 ### pseudocode :

 <pre>
Set count=0,0,i=left,j=mid. C is the sorted list.
Traverse list1 and list2 until mid element  or right element is encountered .
     Compare list1[i] and list[j].
         If list1[i]<=list2[j]
            c[k++]=list1[i++]
         else
            c[k++]=list2[j++]
            count = count + mid-i;
add rest elements of list1 and list2 in c.
copy sorted list c back to original list.
return count.
 </pre>

 ** Key  concept : **If an element of list two is greater than an element of list1 than all the remaining element of list1 are greater than this element. That's why we increment count by mid-i. This recursively sort the list and find the number of inversion.

 #### Time complexity : O(n log n)