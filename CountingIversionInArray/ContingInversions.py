def countInversions(array : [int], left : int, right : int) -> int:
    count = 0
    mid = (left + right) // 2

    if left < right:
        count += countInversions(array, left, mid)
        count += countInversions(array, mid + 1, right)
        count += countMergeInversions(array, left, mid + 1, right)

    return count

def countMergeInversions(array : [int], left : int, mid : int, right: int) -> int:
    count = 0
    i, j, k = left, mid, 0
    length = right - left + 1
    sorted_arr = [0] * length

    while i < mid and j <= right:
        if array[i] <= array[j]:
            sorted_arr[k] = array[i]
            i += 1
        else:
            sorted_arr[k] = array[j]
            j += 1
            count += mid - i
        k += 1

    # Check for left over elements in either of the halfs

    while i < mid:
        sorted_arr[k] = array[i]
        k += 1
        i += 1

    while j <= right:
        sorted_arr[k] = array[j]
        j += 1
        k += 1

    # copy back the sorted elements in the list to original list
    k = 0
    while left <= right:
        array[left] = sorted_arr[k]
        k += 1
        left += 1

    return count

# Driver code

n = int(input())
array = list(map(int, input().split()))

print(countInversions(array, 0, n - 1))

