/* collatz sequence :
Its a sequence which starts with a positive integer 'n' and follows 
if previous term is even then do n / 2 
if it is odd do 3 * n + 1 until we reach 1 */

// using Recursion

import java.util.*;
public class CollatzSequence {
    public static void collatz(int N) {
      System.out.print(String.valueOf(N) + " ");
      if (N == 1) return;
      else if (N % 2 == 0){
          collatz(N / 2);
      }
      else {
          collatz(N * 3 + 1);
      }
    }
    public static void main(String[] args) {
      int n = Integer.parseInt(args[0]);// takes command line argument
      collatz(n);
    }
}
