# python3

def are_matching(left, right):
    return (left + right) in ["()", "[]", "{}"]


def find_mismatch(text):
    opening_brackets_stack = []
    count= 0
    for i, next in enumerate(text):
        if next in "([{":
            # Process opening bracket, write your code here
            opening_brackets_stack.append((next, i))

        if next in ")]}":
            # Process closing bracket, write your code here
            if not(len(opening_brackets_stack)):
                return i + 1
            if not(are_matching((opening_brackets_stack.pop())[0], next)):
                return i + 1
        #print(opening_brackets_stack)

    if not(len(opening_brackets_stack)):
        return "Success"
    elif len(opening_brackets_stack) == 1:
        return opening_brackets_stack[0][1] + 1
    else:
        return len(opening_brackets_stack)


def main():
    text = input()
    mismatch = find_mismatch(text)
    # Printing answer, write your code here
    print(mismatch)


if __name__ == "__main__":
    main()
