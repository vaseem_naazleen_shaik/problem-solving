
// Binary Search : recursive algorithm

import java.util.*;
public class BinarySearch {
    public static int binarySearch(int[] arr, int key, int start, int end) {
        int mid = start + (end - start) / 2;
        while (start <= end) {
            if (arr[mid] == key) {
                return mid + 1;
        }
            else if (arr[mid] < key) {
                return binarySearch(arr, key, mid + 1, end);
        }
             else {
                return binarySearch(arr, key, start, mid - 1);
        }
        }
        return -1;
    
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // reading size of the array
        System.out.println("Enter length of the array:");
        int n = sc.nextInt();
         // reading n elemnts of array
        System.out.println("Enter elements of the array:");
        int[] arr = new int[n];
        
        for (int i = 0; i < n; i ++) {
            arr[i] = sc.nextInt();
        }
        System.out.println("Enter element to search:");
        // element to find
        int elem = sc.nextInt();

        System.out.println(binarySearch(arr, elem, 0, n - 1));
    }
}
